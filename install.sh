# For Fedora
# Dependancies
sudo dnf update
sudo dnf install g++
sudo dnf install python3-devel
sudo dnf install alsa-lib
sudo dnf install alsa-lib-devel
sudo dnf install jack-audio-connection-kit-devel
sudo dnf install ffmpeg
pip3 install --upgrade pip
pip3 install mido
pip3 install pydub
echo "To start odj : python main.py"
echo "Don't forget to prepare your preset directory.."
